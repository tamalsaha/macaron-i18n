module gitea.com/macaron/i18n

go 1.11

require (
	gitea.com/macaron/macaron v1.3.3-0.20190803174002-53e005ff4827
	github.com/Unknwon/com v0.0.0-20190321035513-0fed4efef755
	github.com/Unknwon/i18n v0.0.0-20171114194641-b64d33658966
	github.com/smartystreets/goconvey v0.0.0-20190731233626-505e41936337
	golang.org/x/text v0.3.2
)
